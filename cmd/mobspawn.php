<?php 
// Спаун мобов

// require_once $_SERVER['DOCUMENT_ROOT'] . '/mud/mysqlclass.php';	// Подключение класса работы с БД

require_once $_SERVER['DOCUMENT_ROOT'] . '/mud/conf.php';	// Подключение конфига БД

// получение из БД список спауна

$link = mysqli_connect($base_name, $base_user, $base_pass, $db_name);

$query = "SELECT ms.mtypeid, ms.roomspawn, ms.amount, r.id as rid, r.name as rname, r.x as rx, r.y as ry, mt.name as mname, mt.maxhp, m.id as mid FROM mob_spawn ms LEFT JOIN rooms r ON ms.roomspawn = r.id LEFT JOIN mob_type mt ON ms.mtypeid = mt.id LEFT JOIN mob m ON m.type = ms.mtypeid AND m.x = r.x AND m.y = r.y";

if ($result = mysqli_query($link, $query)) {
	$count = 0;
	// перебор результата по строкам
    while ($tablemobspawn = mysqli_fetch_assoc($result)) {
    	$count++;
    	// $json[$count][]=$tablemobspawn;

    	// проверка по циклу
    	if ($tablemobspawn["mid"]==NULL) {
    		// если моб не заспаунился
    		$json[$count]["mobspawn"]="no";
    		// создаю моба
    		$mtype = $tablemobspawn["mtypeid"];
    		$rx = $tablemobspawn["rx"];
    		$ry = $tablemobspawn["ry"];
    		$maxhp = $tablemobspawn["maxhp"];

    		$mysqlbaseupdate = "INSERT INTO mob (type, x, y, hp) values('".$mtype."','".$rx."','".$ry."','".$maxhp."')";

    		mysqli_query($link, $mysqlbaseupdate);
    	} else {
    		// если моб заспаунился
    		$json[$count]["mobspawn"]="yes";

    	}
    }
}



echo json_encode($json);
?>