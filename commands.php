<?php 
// Список команд сервера

// require_once "func/strtolower_utf8.php"; // подключение функции преобразования кодировки

$input1 = mb_convert_case($input1, MB_CASE_LOWER, "UTF-8");

switch ($input1) :
// северы
	case 'север':
	case 'с':
	case 'се':
	case 'сев':
	case 'севе':
	case 'n':
	case 'north':
		require "cmd/walk_north.php"; // подключение файла команды
		break;

// юг
	case 'юг':
	case 'ю':
	case 's':
	case 'south':
		require "cmd/walk_south.php"; // подключение файла команды
		break;

// запад
	case 'запад':
	case 'з':
	case 'за':
	case 'зап':
	case 'запа':
	case 'west':
	case 'w':
		require "cmd/walk_west.php"; // подключение файла команды
		break;

// восток
	case 'восток':
	case 'в':
	case 'во':
	case 'вос':
	case 'вост':
	case 'восто':
	case 'e':
	case 'east':
		require "cmd/walk_east.php"; // подключение файла команды
		break;

////
	case 'смотреть':
	case 'см':
	case 'смо':
	case 'смот':
	case 'смотр':
	case 'смотре':
	case 'смотрет':
	case 'look':
	case 'l':
		require "cmd/look.php"; // подключение файла команды
		break;

	case 'моб':
		require "cmd/mobspawn.php";
		break;


// ****
// Если команда, введенная пользователем не найдена
	default:
		$json["message"] = "0:1";	// код "команда не найдена"
		echo json_encode($json);
endswitch;

?>