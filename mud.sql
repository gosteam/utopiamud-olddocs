-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 12 2014 г., 19:54
-- Версия сервера: 5.5.34-0ubuntu0.13.10.1-log
-- Версия PHP: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `mud`
--

-- --------------------------------------------------------

--
-- Структура таблицы `mob`
--

CREATE TABLE IF NOT EXISTS `mob` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `x` int(5) NOT NULL,
  `y` int(5) NOT NULL,
  `hp` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `mob`
--

INSERT INTO `mob` (`id`, `type`, `x`, `y`, `hp`) VALUES
(1, 1, -1, 0, 10),
(2, 1, 0, 1, 10),
(3, 1, 0, 1, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `mob_spawn`
--

CREATE TABLE IF NOT EXISTS `mob_spawn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomspawn` int(11) NOT NULL,
  `mtypeid` int(11) NOT NULL,
  `amount` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `mob_spawn`
--

INSERT INTO `mob_spawn` (`id`, `roomspawn`, `mtypeid`, `amount`) VALUES
(1, 5, 1, 1),
(2, 2, 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `mob_type`
--

CREATE TABLE IF NOT EXISTS `mob_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `ename` varchar(15) NOT NULL,
  `sdesc` varchar(30) NOT NULL,
  `ldesc` varchar(100) NOT NULL,
  `maxhp` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `mob_type`
--

INSERT INTO `mob_type` (`id`, `name`, `ename`, `sdesc`, `ldesc`, `maxhp`) VALUES
(1, 'моб', 'mob', 'тестовый моб', 'Тестовый моб стоит тут', 10);

-- --------------------------------------------------------

--
-- Структура таблицы `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `x` int(5) NOT NULL,
  `y` int(5) NOT NULL,
  `roomdesc` varchar(500) NOT NULL DEFAULT 'Ничем не примечательное место.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `x`, `y`, `roomdesc`) VALUES
(1, 'Центр Мироздания', 0, 0, 'Начальная точка отсчета нашего мира, а так же и последняя его граница.'),
(2, 'Северный полюс', 0, 1, 'Ничем не примечательное место.'),
(3, 'Южный полюс', 0, -1, 'Ничем не примечательное место.'),
(4, 'Восточный край', 1, 0, 'Ничем не примечательное место.'),
(5, 'Западный край', -1, 0, 'Ничем не примечательное место.'),
(6, 'Технический коридор', 1, 1, 'Ничем не примечательное место.'),
(7, 'Технический коридор', -1, -1, 'Ничем не примечательное место.'),
(8, 'Технический коридор', -1, 1, 'Ничем не примечательное место.'),
(9, 'Технический коридор', 1, -1, 'Ничем не примечательное место.');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(12) NOT NULL,
  `x` int(5) NOT NULL DEFAULT '0',
  `y` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `x`, `y`) VALUES
(1, 'testman', 0, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
