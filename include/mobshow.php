<?php
// отображение мобов в комнатах

require_once $_SERVER['DOCUMENT_ROOT'] . '/mud/conf.php';	// Подключение конфига БД

// получение из БД список мобов в комнате
$link = mysqli_connect($base_name, $base_user, $base_pass, $db_name);

$query = "SELECT m.id as mid, m.x as mx, m.y as my, mt.ldesc, mt.name FROM mob m LEFT JOIN mob_type mt ON m.type = mt.id LEFT JOIN rooms r ON m.x = r.x AND m.y = r.y WHERE m.x = $x AND m.y = $y";

if ($result = mysqli_query($link, $query)) {
	$count = 0;
	// перебор результата по строкам
    while ($tablemobshow = mysqli_fetch_assoc($result)) {
    	// если в комнате есть мобы
    	$count++;
    	$json["mobs"][$count]["ldesc"]=$tablemobshow["ldesc"];
        $json["mobs"][$count]["name"]=$tablemobshow["name"];
        $json["mobs"][$count]["mid"]=$tablemobshow["mid"];
    }
    if ($count==0) {
    	// если в комнате нет мобов
    	$json["mobshere"]="no";
    } else {
    	// если есть, то сколько
    	$json["howmanymobshere"]=$count;
    }
}

?>