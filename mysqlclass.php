<?php 
header("charset=utf-8");
include_once 'conf.php'; //подключаем файл с параметрами БД

/* Подключаемся к БД, если не получаеться, то выводим ошибку */
$data = mysql_connect($base_name, $base_user, $base_pass);
if(!mysql_select_db($db_name,$data)){
               echo "<BR>";
               echo mysql_error();
               die();
}
mysql_close($data);


/**
* Класс для работы с БД
*
* selfrom("название_таблицы", "поля_для_выборки"); -- Возвращает массив значений
* getval("название_таблицы", "поля_для_выборки"); -- Возвращает одно значение
* setid("название таблицы", "значение", "новое_значение"); -- Возвращает true
* set("название таблицы", "значение", "новое_значение", "фильтр_ячейки_where", "значение_фильтра"); -- Возвращает true
*/
class MySQL
{
	// public $id = $_SESSION['user_id'];
	public $id = 1;
	protected $base_name;
	public $base_user;
	protected $base_pass;
	protected $db_name;

	function __construct() {
		$this->base_name = $GLOBALS['base_name'];
		$this->base_user = $GLOBALS['base_user'];
		$this->base_pass = $GLOBALS['base_pass'];
		$this->db_name = $GLOBALS['db_name'];
	}

	// Подключение к БД
	public function con()
	{
		
		// Соединение с БД
		$sql = mysql_connect($this->base_name, $this->base_user, $this->base_pass);
		$sql_select = mysql_select_db($this->db_name);

		return $sql;
	}

	// Выбор таблицы, запрос
	// Возвращает массив результатов запроса
	public function selfrom($from, $select)
	{
		$query = "SELECT $select FROM $from WHERE id = '$this->id'";
		$sqlresult = mysql_query($query,$this->con());
		mysql_close();
		$table = mysql_fetch_assoc($sqlresult);
		return $table;
	}

	// Выбор из БД по одному значению
	public function selfrom1($from, $select, $where, $whereval)
	{
		$query = "SELECT $select FROM $from WHERE `$where` = $whereval";
		$sqlresult = mysql_query($query,$this->con());
		mysql_close();
		$table = mysql_fetch_assoc($sqlresult);
		return $table;
	}

	// Выбор из БД по полученному значению
	public function sel($query)
	{
		$sqlresult = mysql_query($query,$this->con());
		// если найдено хотя бы одно значение
		if (mysql_num_rows($sqlresult)) {
			$table = mysql_fetch_assoc($sqlresult);
			return $table;
		} else {
			return false;
		}
		mysql_close();
	}

	// Выбор из БД по двум значениям
	public function selfrom2($from, $select, $where1, $val1, $where2, $val2)
	{
		$query = "SELECT $select FROM $from WHERE `$where1` = $val1 AND `$where2` = $val2";
		$sqlresult = mysql_query($query,$this->con());
		// если найдено хотя бы одно значение
		if (mysql_num_rows($sqlresult)) {
			$table = mysql_fetch_assoc($sqlresult);
			return $table;
		} else {
			return false;
		}
		mysql_close();
	}

	// Выбор из БД по 3 значениям
	public function selfrom3($from, $select, $where1, $val1, $where2, $val2, $where3, $val3)
	{
		$query = "SELECT $select FROM $from WHERE `$where1` = $val1 AND `$where2` = $val2 AND `$where3` = $val3";
		$sqlresult = mysql_query($query,$this->con());
		// если найдено хотя бы одно значение
		if (mysql_num_rows($sqlresult)) {
			$table = mysql_fetch_assoc($sqlresult);
			return $table;
		} else {
			return false;
		}
		mysql_close();
	}

	// Выбор из БД по 4 значениям
	public function selfrom4($from, $select, $where1, $val1, $where2, $val2, $where3, $val3, $where4, $val4)
	{
		$query = "SELECT $select FROM $from WHERE `$where1` = $val1 AND `$where2` = $val2 AND `$where3` = $val3 AND `$where4` = $val4";
		$sqlresult = mysql_query($query,$this->con());
		// если найдено хотя бы одно значение
		if (mysql_num_rows($sqlresult)) {
			$table = mysql_fetch_assoc($sqlresult);
			return $table;
		} else {
			return false;
		}
		mysql_close();
	}

	// Выбор одного значения из БД
	// Возвращает выбранное значение
	public function getval($from, $select)
	{
		$table = $this->selfrom($from, $select);
		return $table[$select];
	}

	// Запись данных в БД относительно id
	// Возвращает true
	public function setid($table, $val, $newval)
	{
		$query = "UPDATE `$this->db_name`.`$table` SET `$val` = '$newval' WHERE `$table`.`id` = $this->id";
		mysql_query($query,$this->con());
		mysql_close();

		return 1;
	}

	// Запись данных в БД
	// Возвращает true
	public function set($table, $val, $newval, $where, $whereval)
	{
		$query = "UPDATE `$this->db_name`.`$table` SET `$val` = '$newval' WHERE `$table`.`$where` = $whereval";
		mysql_query($query,$this->con());
		mysql_close();

		return 1;
	}

}


// // Создание объекта для работы с БД
// $mysql = new MySQL;

// // Выбор и вывод значения из БД
// echo $mysql->getval("users", "name");


?>