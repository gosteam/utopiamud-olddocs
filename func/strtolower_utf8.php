<?php

// Приведение кириллицы к нижнему регистру
function strtolower_my($string) {
	$string = mb_convert_case($string, MB_CASE_LOWER, "UTF-8"); 
	return $string; 
}

?>