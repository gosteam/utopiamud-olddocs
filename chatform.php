<?php
# Подключаем файл с параметрами БД
include_once "conf.php";

# Подключаемся к БД, если не получаеться, то выводим ошибку
$data = mysql_connect($base_name, $base_user, $base_pass);
if (!mysql_select_db($db_name,$data)) {
	// $json = array(
	// 	'message'=>"Ошибка соединения с базой данных (обратитесь к администратору сервера)."
	// );
	$json["message"] = "0:2";	// код "ошибка соединения с базой данных"
	echo json_encode($json);
	die();
}

// Ввод пользователя
$inputall = strip_tags($_POST['chat']);	// <input name="chat">
// Разбитие ввода на отдельные слова
$input = explode(' ',trim($inputall));
$input1 = $input[0]; // первое слово команды
$input2 = $input[1]; // второе слово команды

// Команды

require_once "commands.php"; // подключение файла команд

// Если ничего не произошло
if (!$json) {
	$json = array(
		'message'=>"Ошибка."
	);
	echo json_encode($json);
}

?>